import { Component, OnInit } from '@angular/core';
import { Pet } from 'src/app/models/pet.model';
import { PetService } from 'src/app/services/pet.service';

@Component({
  selector: 'app-tutorials-list',
  templateUrl: './pets-list.component.html',
  styleUrls: ['./pets-list.component.css']
})
export class PetsListComponent implements OnInit {

  pets?: Pet[];
  currentPet: Pet = {};
  currentIndex = -1;
  name = '';

  constructor(private petService: PetService) { }

  ngOnInit(): void {
    this.retrieveTutorials();
  }

  retrieveTutorials(): void {
    this.petService.getAll()
      .subscribe({
        next: (data) => {
          this.pets = data;
          console.log(data);
        },
        error: (e) => console.error(e)
      });
  }

  refreshList(): void {
    this.retrieveTutorials();
    this.currentPet = {};
    this.currentIndex = -1;
  }

  setActiveTutorial(tutorial: Pet, index: number): void {
    this.currentPet = tutorial;
    this.currentIndex = index;
  }

  removeAllTutorials(): void {
    this.petService.deleteAll()
      .subscribe({
        next: (res) => {
          console.log(res);
          this.refreshList();
        },
        error: (e) => console.error(e)
      });
  }

  searchTitle(): void {
    this.currentPet = {};
    this.currentIndex = -1;

    this.petService.findByTitle(this.name)
      .subscribe({
        next: (data) => {
          this.pets = data;
          console.log(data);
        },
        error: (e) => console.error(e)
      });
  }

}
