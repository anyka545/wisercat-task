import { Component } from '@angular/core';
import { Pet } from 'src/app/models/pet.model';
import { PetService } from 'src/app/services/pet.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-add-tutorial',
  templateUrl: './add-pet.component.html',
  styleUrls: ['./add-pet.component.css']
})

export class AddPetComponent {
  pet: Pet = {
    name: '',
    code: '',
    type: '',
    furColor: '',
    country: ''
  };
  submitted = false;

  constructor(private tutorialService: PetService) { }

  saveTutorial(): void {
    const data = {
      name: this.pet.name,
      code: this.pet.code,
      type: this.pet.type,
      furColor: this.pet.furColor,
      country: this.pet.country,
    };

    this.tutorialService.create(data)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.submitted = true;
        },
        error: (e) => console.error(e)
      });
  }

  newTutorial(): void {
    this.submitted = false;
    this.pet = {
      name: '',
      code: '',
      type: '',
      furColor: '',
      country: ''
    };
  }

}
