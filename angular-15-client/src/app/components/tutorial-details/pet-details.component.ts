import { Component, Input, OnInit } from '@angular/core';
import { PetService } from 'src/app/services/pet.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Pet } from 'src/app/models/pet.model';

@Component({
  selector: 'app-tutorial-details',
  templateUrl: './pet-details.component.html',
  styleUrls: ['./pet-details.component.css']
})
export class PetDetailsComponent implements OnInit {

  @Input() viewMode = false;

  @Input() currentPet: Pet = {
    name: '',
    code: '',
    type: '',
    furColor: '',
    country: ''
  };

  message = '';

  constructor(
    private petService: PetService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    if (!this.viewMode) {
      this.message = '';
      this.getTutorial(this.route.snapshot.params["id"]);
    }
  }

  getTutorial(id: string): void {
    this.petService.get(id)
      .subscribe({
        next: (data) => {
          this.currentPet = data;
          console.log(data);
        },
        error: (e) => console.error(e)
      });
  }


  updateTutorial(): void {
    this.message = '';

    this.petService.update(this.currentPet.id, this.currentPet)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.message = res.message ? res.message : 'This pet was updated successfully!';
        },
        error: (e) => console.error(e)
      });
  }

  deleteTutorial(): void {
    this.petService.delete(this.currentPet.id)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.router.navigate(['/pets']);
        },
        error: (e) => console.error(e)
      });
  }

}
