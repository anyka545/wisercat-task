package com.bezkoder.spring.jpa.h2.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.bezkoder.spring.jpa.h2.repository.TutorialRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bezkoder.spring.jpa.h2.model.Pet;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class TutorialController {

	@Autowired
	TutorialRepository tutorialRepository;

	@GetMapping("/pets")
	public ResponseEntity<List<Pet>> getAllPets(@RequestParam(required = false) String name) {
		try {
			List<Pet> pets = new ArrayList<Pet>();

			if (name == null)
				tutorialRepository.findAll().forEach(pets::add);
			else
				tutorialRepository.findByNameContaining(name).forEach(pets::add);

			if (pets.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(pets, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/pets/{id}")
	public ResponseEntity<Pet> getTutorialById(@PathVariable("id") long id) {
		Optional<Pet> tutorialData = tutorialRepository.findById(id);

		if (tutorialData.isPresent()) {
			return new ResponseEntity<>(tutorialData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/pets")
	public ResponseEntity<Pet> createTutorial(@RequestBody Pet pet) {
		try {
			Pet _pet = tutorialRepository
					.save(new Pet(pet.getName(), pet.getCode(), pet.getType(), pet.getColor(), pet.getCountry()));
			return new ResponseEntity<>(_pet, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/pets/{id}")
	public ResponseEntity<Pet> updateTutorial(@PathVariable("id") long id, @RequestBody Pet pet) {
		Optional<Pet> tutorialData = tutorialRepository.findById(id);

		if (tutorialData.isPresent()) {
			Pet _petl = tutorialData.get();
			_petl.setName(pet.getName());
			_petl.setCode(pet.getCode());
			_petl.setType(pet.getType());
			_petl.setColor(pet.getColor());
			_petl.setCountry(pet.getCountry());
			return new ResponseEntity<>(tutorialRepository.save(_petl), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/pets/{id}")
	public ResponseEntity<HttpStatus> deleteTutorial(@PathVariable("id") long id) {
		try {
			tutorialRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/pets")
	public ResponseEntity<HttpStatus> deleteAllTutorials() {
		try {
			tutorialRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}


}
