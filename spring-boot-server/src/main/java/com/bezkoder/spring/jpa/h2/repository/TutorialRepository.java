package com.bezkoder.spring.jpa.h2.repository;

import java.util.List;

import com.bezkoder.spring.jpa.h2.model.Pet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TutorialRepository extends JpaRepository<Pet, Long> {

  List<Pet> findByNameContaining(String name);
}
