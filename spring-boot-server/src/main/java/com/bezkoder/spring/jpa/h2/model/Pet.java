package com.bezkoder.spring.jpa.h2.model;

import javax.persistence.*;

@Entity
@Table(name = "pets")
public class Pet {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "name")
	private String name;

	@Column(name = "code")
	private String code;

	@Column(name = "type")
	private String type;

	@Column(name = "color")
	private String color;

	@Column(name = "country")
	private String country;

	public Pet() {

	}

	public Pet(String name, String code, String type,String color,String country) {
		this.name= name;
		this.code = code;
		this.color = color;
		this.type = type;
		this.country = country;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}


	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.type = country;
	}

	@Override
	public String toString() {
		return "Pet [id=" + id + ", name=" + name + ", code=" + code + ", type=" + type + ", color=" + color + ", country=" + country +"]";
	}

}
